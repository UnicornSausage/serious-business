// Main header script - this will be included into every script in
// the game (local and global). Do not place functions here; rather,
// place import definitions and #define names here to be used by all
// scripts.

import function walk_toRoom(this Character*, int roomNumber,  int leave_x,  int leave_y,  int arrive_x,  int arrive_y,  const string facing);
import function walk_onRegion(this Character*,  int region_id);
import function goTo_Bank(this Character*);
import function goTo_Fatboys(this Character*);
import function wander(this Character*);
import function enter_fatboys(this Character*);
import function check_Dayphase();
import function createQuest(String questTitle, String questDesc);
import function createTask(String pq, String taskDesc);
import function setVersion(String release);
import function showStudio();

struct Task {
  String desc;
  String parentQuest;
  bool complete;
  import function Complete();
  int count;
  int ID;
};

struct Quest {
  String title;
  String desc;
  bool complete;
  import function Complete();
};

import Quest quest[60];
import Task task[100];