## Serious Business

WorldCrash: Serious Business is the first entry in the WORLDCRASH series, a post-post-post-apocalyptic look at human evolution gone completely wrong.

### The plot:

**Note: for a breakdown of the story world and its lore, [check out the wiki](https://git.feneas.org/deadsuperhero/boring-game/-/wikis/home)**

A massive economic crisis and ecological destabilization has pushed society into a dramatic and vicious cycle of de-evolution.
Hundreds of years have passed since the initial event, which everyone has forgotten about.

You are Dan, a lowly accountant, and part of the producer class of this bold new world. Dan feels uncomfortable with his existence, and feels conflicted about a rising pressure to fit in.
As time goes on, Dan uncovers a conspiracy, and realizes that his society has been trapped in a regressive cycle of development.

### Special Thanks - AGS Community Modules

This game uses several community modules for Adventure Game Studio to profoundly extend the engine's core functionality. I'm giving credit where credit is due:

* [CharacterControl Script Module](https://www.adventuregamestudio.co.uk/forums/index.php?topic=28821.0) by [Strazer](https://www.adventuregamestudio.co.uk/forums/index.php?action=profile;u=1843).
This module helps with the mass-assignment of command chains that NPCs execute. In plain English: this gives me a way to make NPCs follow a long set of instructions based on whatever random set of conditions I want,
giving in-game NPC's the illusion of being more life-like.
* [AGS Tween](https://www.adventuregamestudio.co.uk/forums/index.php?topic=57315.0) by [Edmundito](https://www.adventuregamestudio.co.uk/forums/index.php?action=profile;u=45). This module lets me take advantage of easing functions for animations, meaning that I can make things neatly fade in and out or slide up without having to do too much extra work.
* [AGS Alternative Timer](https://www.adventuregamestudio.co.uk/forums/index.php?topic=55545.msg636635016) by [Crimson Wizard](https://www.adventuregamestudio.co.uk/forums/index.php?action=profile;u=8413). The default Timer system in AGS is really hard to work with, and AGS Timer lets me instead have way more functionality.
* [Day/Night](https://www.adventuregamestudio.co.uk/forums/index.php?topic=49791.msg636477685) by [Snarky](https://www.adventuregamestudio.co.uk/forums/index.php?action=profile;u=2581). This helps me implement a day-clock with different phases of the day. Currently, it's being used to explore the
possibility of a day/midday/night cycle, kind of like how Quest for Glory used to do it.

I hope that this game's source code illustrates how the script modules can  be used - the game takes place in a city,
so there are many potential use-cases that this can apply to!

## Note

Currently the game is a sandbox; there really isn't much to do in terms of actual plot. The goal right now is to instead explore building
out the game's world and systems as a sort of cohesive framework. In other words, the game currently serves the purpose of generating
an asset library for sprites, animations, and code.

### Running the game

Currently, this game is compiled for Windows and Linux, and we hope to also include the AGS runtime for macOS as well.

#### Windows

1. Open the `Compiled` Folder
2. Navigate to `Windows`
3. Click `worldcrash-serious-business.exe`


#### Linux

1. Open the `Compiled` Folder
2. Navigate to `Linux`
3. Run `./worldcrash-serious-business`, the correct libraries will be selected for either 32-bit or 64-bit.

If you have difficulties running the executable, try navigating to the folder directly in the terminal, and run `./worldcrash-serious-business` manually. 
You *may* have to perform `chmod +x` on the executable, along with the `ags32` and/or `ags64` binaries in the data folder.
