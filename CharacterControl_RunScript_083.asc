// Main script for module 'CharacterControl RunScript'

//====================================================================================================
function CharacterControl_RunScript(Character *thechar, String parameter1, String parameter2, String parameter3, String parameter4) {
// Put your custom scripts for the RUN control command in here.
// This function is called when a character executes the RUN control command.

// You can perform actions on the executing character via the thechar pointer, for example:
//   thechar.Think("Impressive...");

	thechar.UnlockView(); // unlock the character's view in case an animation was played before

	//--------------------------------------------------
	// Example
	//--------------------------------------------------
	if (parameter1.AsInt == 28) { // if "RUN:28;" used
		thechar.SayBackground("It sure is hot today!"); // display some speech
		return 80; // wait 80 game loops (2 seconds) before executing next command in chain
	}
	//--------------------------------------------------

}
//====================================================================================================
