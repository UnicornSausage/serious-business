// Script header for module 'CharacterControl RunScript'

import function CharacterControl_RunScript(Character *thechar, String parameter1, String parameter2, String parameter3, String parameter4); // $AUTOCOMPLETEIGNORE$

//****************************************************************************************************
// VERSION
//****************************************************************************************************

#define CharacterControl_RunScript_VERSION 83// this version
#define CharacterControl_RunScript_VERSION_083// provides v0.83 functionality
//#define CharacterControl_RunScript_VERSION_080 // not compatible with v0.80

//****************************************************************************************************
// DEPENDENCY CHECKS
//****************************************************************************************************

#ifndef AGS_NEW_STRINGS
	#error This module requires AGS v2.71 or later.
#endif
