// Script header for module 'CharacterControl'

struct CharacterControl {
	import static function CreateChain(int chainid, String commandstring);
	import static function AppendChain(int chainid, String commandstring);
	import static function GetChain(Character *thechar);
	import static function GetChainPosition(Character *thechar);
	import static function GetRoom(Character *thechar);
	import static bool IsExecuting(Character *thechar);
	import static bool IsPaused(Character *thechar);
	import static function PauseExecuting(Character *thechar);
	import static function ResumeExecuting(Character *thechar);
	import static function StartExecuting(Character *thechar, int chainid, int chainposition=1);
	import static function StopExecuting(Character *thechar);
};

//****************************************************************************************************
// VERSION
//****************************************************************************************************

#ifdef AGS_SUPPORTS_IFVER
  #ifver 3.0
    #define CharacterControl_VERSION 0.85// this version
    #define CharacterControl_VERSION_085// provides v0.85 functionality
  #endif
#endif

//****************************************************************************************************
// DEPENDENCY CHECKS
//****************************************************************************************************

#ifndef CharacterControl_VERSION
  #error CharacterControl module error: This module requires AGS version 3.0 or higher. To use the module with a prior AGS version, please use an older version of the module.
#endif

#ifndef CharacterControl_RunScript_VERSION_083
  #error CharacterControl module error: This module requires at least v0.83 of the CharacterControl RunScript module.
#endif
